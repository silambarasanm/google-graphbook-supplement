###########################################################################
# Copyright (C) 2013 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# NOTE: This script should be run from Python.
#
# Some basic statistics for a graph.

import argparse
import sys

###################################
# helper functions
###################################

def statistics(input):
    """
    Some basic statistics for a graph.

    - input -- read in the edge list from this file, one edge per line.
    """
    V = set()  # set of nodes
    N = 0      # how many edges; don't count self-loops
    f = open(input, "r")
    for line in f:
        u = None
        v = None
        if len(line.strip().split(" ")) > 2:
            u, v, _ = line.strip().split(" ")
        else:
            u, v = line.strip().split(" ")
        if u not in V:
            _ = V.add(u)
        if v not in V:
            _ = V.add(v)
        if u == v:
            continue
        N += 1
    f.close()
    print "Nodes: %d" % len(V)
    print "Edges: %d" % N
    sys.stdout.flush()

###################################
# script starts here
###################################

# setup parser for command line options
s = "Some basic statistics for a graph.\n"
parser = argparse.ArgumentParser(description=s)
parser.add_argument("--input", metavar="file", required=True,
                    help="read in the edge list from this file")
args = parser.parse_args()
# get the command line arguments
input = args.input

statistics(input)
